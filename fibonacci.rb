require "benchmark"


def fibs_iterative(n)
    (1...n).inject([1,1]){ |ary| ary.push(ary.last(2).inject(:+)) }
end


def fibs_binet(n)
    (1..n).map { |i| ((((1 + 5**0.5) / 2) ** i  -  ((1 - 5**0.5) / 2) ** i) / 5**0.5).round }
end


def fibs_recursive(n)
    n == 2 ? [1,1] : fibs_recursive(n-1).push(fibs_recursive(n-1).last(2).inject(:+))
end


n = 20
result_iterative = nil
result_binet     = nil
result_recursive = nil

Benchmark.bm(20) do |x|
    x.report("fibs_iterative(#{n}):") { result_iterative = fibs_iterative(n) }
    x.report("fibs_binet(#{n}):")     { result_binet     = fibs_binet(n)     }
    x.report("fibs_recursive(#{n}):") { result_recursive = fibs_recursive(n) }
end

puts ""
puts "fibs_iterative(#{n}) = #{result_iterative}"
puts "fibs_binet(#{n})     = #{result_iterative}"
puts "fibs_recursive(#{n}) = #{result_iterative}"